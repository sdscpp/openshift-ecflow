# OpenShift DIY Cartridge
This cartridge is documented in the [Cartridge Guide](http://openshift.github.io/documentation/oo_cartridge_guide.html#diy).

# See the better documentation
[Cartridge developers guide](https://github.com/openshift/origin-server/blob/master/documentation/oo_cartridge_developers_guide.adoc#endpoints)

# And a discussion that includes how to use it 
[New openshift format](https://blog.openshift.com/new-openshift-cartridge-format-part-2/)

# Install using
    rhc create-app <name> https://bitbucket.org/sdscpp/openshift-ecflow/raw/master/metadata/manifest.yml
